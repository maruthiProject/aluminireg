package com.jp.aluminiregards.model;

import java.io.Serializable;

public class StudentModel implements Serializable {

    String userId,studentName,studentPassword,address,email,date,yearPasssed,companyHr,mobileNo,dob,depart;
    byte[] image;

    public StudentModel(String userId, String studentName, String studentPassword, String address, String email, String yearPasssed, String companyHr, String mobileNo, byte[] image, String dob,String depart) {
        this.userId = userId;
        this.studentName = studentName;
        this.studentPassword = studentPassword;
        this.address = address;
        this.email = email;
        this.yearPasssed = yearPasssed;
        this.companyHr = companyHr;
        this.mobileNo = mobileNo;
        this.image = image;
        this.dob = dob;
        this.depart = depart;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentPassword() {
        return studentPassword;
    }

    public void setStudentPassword(String studentPassword) {
        this.studentPassword = studentPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getYearPasssed() {
        return yearPasssed;
    }

    public void setYearPasssed(String yearPasssed) {
        this.yearPasssed = yearPasssed;
    }

    public String getCompanyHr() {
        return companyHr;
    }

    public void setCompanyHr(String companyHr) {
        this.companyHr = companyHr;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
