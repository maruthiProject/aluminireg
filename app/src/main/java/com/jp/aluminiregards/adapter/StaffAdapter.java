package com.jp.aluminiregards.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jp.aluminiregards.R;
import com.jp.aluminiregards.db.DBHelper;
import com.jp.aluminiregards.helper.CommonMethod;
import com.jp.aluminiregards.model.StaffModel;

import java.util.ArrayList;
import java.util.List;

public class StaffAdapter extends Adapter<StaffAdapter.MyViewHolder> implements Filterable {
    private Context context;
    private List<StaffModel> list;
    CommonMethod commonMethods;
    private List<StaffModel> srchlist;
    DBHelper dbHelper;

    public class MyViewHolder extends ViewHolder {
        LinearLayout expectedDateTime;
        TextView addres, timeDifference;
        TextView branchtext;
        TextView checktype;
        ImageView icon;
        TextView mobile;
        TextView name;
        ImageView profpic;
        TextView time, timeOut, expectedDate, expectedTime;
        ImageView visitoricon;

        public MyViewHolder(View view) {
            super(view);
            this.name = view.findViewById(R.id.name);
            this.addres = view.findViewById(R.id.id);
            this.mobile = view.findViewById(R.id.classes);
            this.profpic = view.findViewById(R.id.orderimages);
        }
    }

    public StaffAdapter(Context context, List<StaffModel> list) {
        this.context = context;
        this.list = list;
        commonMethods = new CommonMethod(context);
        this.srchlist = list;
        dbHelper = new DBHelper(context);
    }

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.homelist_demo, parent, false));
    }

    public void onBindViewHolder(MyViewHolder holder, int position) {
        StaffModel model = this.list.get(position);
        holder.name.setText(model.getTitle());
        holder.addres.setText(model.getMessage());
        if (model.getImage() != null) {
            commonMethods.imageView(holder.profpic, model.getImage(), context);
        } else {
            holder.profpic.setImageDrawable(context.getResources().getDrawable(R.drawable.user_male));
        }
        //Common Method call
    }

    public int getItemCount() {
        Log.e("TAG", "getItemCount: "+list.size() );
        return this.list.size();
    }

    public Filter getFilter() {
        return new Filter() {
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    StaffAdapter.this.list = StaffAdapter.this.srchlist;
                } else {
                    ArrayList<StaffModel> filteredList = new ArrayList();
                    for (StaffModel androidVersion : StaffAdapter.this.srchlist) {
                        if (androidVersion.getTitle().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }
                    StaffAdapter.this.list = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = StaffAdapter.this.list;
                return filterResults;
            }

            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                StaffAdapter.this.list = (ArrayList) filterResults.values;
                StaffAdapter.this.notifyDataSetChanged();
            }
        };
    }
}
