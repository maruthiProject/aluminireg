package com.jp.aluminiregards.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jp.aluminiregards.R;
import com.jp.aluminiregards.db.DBHelper;
import com.jp.aluminiregards.helper.CommonMethod;
import com.jp.aluminiregards.model.StudentModel;

import java.util.ArrayList;
import java.util.List;

public class StaffStudentAdapter extends Adapter<StaffStudentAdapter.MyViewHolder> implements Filterable {
    private Context context;
    private List<StudentModel> list;
    CommonMethod commonMethods;
    private List<StudentModel> srchlist;
    DBHelper dbHelper;
    private String TAG=StaffStudentAdapter.class.getSimpleName();

    public class MyViewHolder extends ViewHolder {
        LinearLayout expectedDateTime;
        TextView addres, attendance;
        ImageView icon;
        TextView mobile;
        TextView name;
        ImageView profpic;
        TextView time, timeOut, expectedDate, expectedTime;
        ImageView visitoricon;

        public MyViewHolder(View view) {
            super(view);
            this.name = view.findViewById(R.id.name);
            this.addres = view.findViewById(R.id.id);
            this.mobile = view.findViewById(R.id.classes);
            this.profpic = view.findViewById(R.id.orderimages);
            this.attendance=view.findViewById(R.id.attendance);
        }
    }

    public StaffStudentAdapter(Context context, List<StudentModel> list) {
        this.context = context;
        this.list = list;
        commonMethods = new CommonMethod(context);
        this.srchlist = list;
        dbHelper = new DBHelper(context);
    }

    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.homelist_demo1, parent, false));
    }

    public void onBindViewHolder(MyViewHolder holder, int position) {
        StudentModel model = this.list.get(position);
        holder.name.setText(model.getStudentName());
        holder.addres.setText(model.getAddress());
        holder.mobile.setText(model.getMobileNo());
        if (model.getImage() != null) {
            commonMethods.imageView(holder.profpic, model.getImage(), context);
        } else {
            holder.profpic.setImageDrawable(context.getResources().getDrawable(R.drawable.user_male));
        }
       /* try{
            Log.e(TAG, "onBindViewHolder: "+model.getAttendance() );
        }catch (NullPointerException e){
            Log.e(TAG, "onBindViewHolder: "+e.getLocalizedMessage() );
        }*/
      /*  if (model.getAttendance().equals("A")){
            holder.attendance.setBackground(context.getResources().getDrawable(R.drawable.text_red_boder));
            holder.attendance.setText(model.getAttendance());
        }else {
            holder.attendance.setBackground(context.getResources().getDrawable(R.drawable.text_green_boder));
            holder.attendance.setText(model.getAttendance());
        }*/
        //Common Method call
    }

    public int getItemCount() {
        Log.e("TAG", "getItemCount: "+list.size() );
        return this.list.size();
    }

    public Filter getFilter() {
        return new Filter() {
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    StaffStudentAdapter.this.list = StaffStudentAdapter.this.srchlist;
                } else {
                    ArrayList<StudentModel> filteredList = new ArrayList();
                    for (StudentModel androidVersion : StaffStudentAdapter.this.srchlist) {
                        if (androidVersion.getStudentName().toLowerCase().contains(charString)) {
                            filteredList.add(androidVersion);
                        }
                    }
                    StaffStudentAdapter.this.list = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = StaffStudentAdapter.this.list;
                return filterResults;
            }

            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                StaffStudentAdapter.this.list = (ArrayList) filterResults.values;
                StaffStudentAdapter.this.notifyDataSetChanged();
            }
        };
    }
}
