package com.jp.aluminiregards.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jp.aluminiregards.R;
import com.jp.aluminiregards.adapter.StudentAdapter;
import com.jp.aluminiregards.db.DBHelper;
import com.jp.aluminiregards.helper.RecyclerTouchListener;
import com.jp.aluminiregards.model.StudentModel;

import java.util.ArrayList;

public class StudentListFragment extends Fragment implements View.OnClickListener {
    View view;
    RecyclerView recyclerViewList;
    StudentAdapter adapter;
    ArrayList<StudentModel> studentModelArrayList;
    DBHelper dbHelper;
    TextView noData;
    FloatingActionButton fab;
    private TextView dateSearch;
    private boolean isBackPressed=true;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_staff_list,container,false);
        fab=view.findViewById(R.id.fab);
        fab.setOnClickListener(this);
        recyclerViewList=view.findViewById(R.id.recyclerViewList);
        recyclerViewList.setHasFixedSize(true);
        dbHelper=new DBHelper(getActivity());
        dateSearch=view.findViewById(R.id.dateSearch);
        dateSearch.setVisibility(View.GONE);
        noData=view.findViewById(R.id.noData);
        fab.setVisibility(View.GONE);
        recyclerViewList.setLayoutManager(new LinearLayoutManager(getActivity()));
        studentModelArrayList=dbHelper.getStudent();
        if (studentModelArrayList.size()>0){
            noData.setVisibility(View.GONE);
            recyclerViewList.setVisibility(View.VISIBLE);
        }
        adapter=new StudentAdapter(getActivity(),studentModelArrayList);
        recyclerViewList.setAdapter(adapter);
        recyclerViewList.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerViewList, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                StudentModel studentModel=studentModelArrayList.get(position);
                setFragment(new StudentRegFragment(),studentModel);
            }

            @Override
            public void onLongClick(View view, int i) {

            }
        }));
        handleBackPress(view);
        return view;
    }


    private void handleBackPress(View view) {

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (!isBackPressed) {
                        setFragment(new AdminHomeFragment());
                        isBackPressed = true;
                    } else if (isBackPressed) {
                        isBackPressed = false;
                    }
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab:
//                setFragment(new StudentRegFragment());
                break;
        }
    }
    private void setFragment(Fragment _fragment,StudentModel studentModel) {
        // create a FragmentManager
        Bundle bundle=new Bundle();
        bundle.putSerializable("model",studentModel );
        FragmentManager fm = getActivity().getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        _fragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.framlayout1, _fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit(); // save the changes
    }
    private void setFragment(Fragment _fragment) {
        // create a FragmentManager
        FragmentManager fm = getActivity().getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.framlayout1, _fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit(); // save the changes
    }
}
