package com.jp.aluminiregards.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jp.aluminiregards.R;

public class AdminHomeFragment extends Fragment {
    View view;
    TextView staff,student;
    private boolean isBackPressed=true;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_admin_home,container,false);
        staff=view.findViewById(R.id.staff);
        student=view.findViewById(R.id.student);
        staff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                staffReg();
            }
        });
        student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                studentReg();
            }
        });
        handleBackPress(view);
        return view;
    }
    private void handleBackPress(View view) {

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (!isBackPressed) {
                       getActivity().finishAffinity();
                        isBackPressed = true;
                    } else if (isBackPressed) {
                        isBackPressed = false;
                    }
                    return true;
                }
                return false;
            }
        });
    }
    public void staffReg(){
        setFragment(new StaffListFragment(),"0");

    }
    public void studentReg(){
        setFragment(new StudentListFragment());
    }
    private void setFragment(Fragment _fragment) {
        // create a FragmentManager
        FragmentManager fm = getActivity().getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.framlayout1, _fragment);

        fragmentTransaction.commit(); // save the changes
    }
    private void setFragment(Fragment fragment, String count) {

        Bundle bundle=new Bundle();
        bundle.putString("count",count);
        fragment.setArguments(bundle);
        // create a FragmentManager
        FragmentManager fm = getActivity().getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.framlayout1, fragment);
        fragmentTransaction.commit(); // save the changes
    }
}
