package com.jp.aluminiregards.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jp.aluminiregards.R;
import com.jp.aluminiregards.db.DBHelper;
import com.jp.aluminiregards.helper.CommonMethod;
import com.jp.aluminiregards.helper.StaticData;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

import id.zelory.compressor.Compressor;

public class StaffRegFragment extends Fragment implements View.OnClickListener {
    View view;
    StaticData staticData;
    ArrayList<String> classes=new ArrayList<>();
    ArrayList<String> classId=new ArrayList<>();
    ArrayAdapter<String> classesAdapter;
    EditText eventTitle,message;
    CommonMethod commonMethod;
    TextView register;
    private int PICK_IMAGE_GALLERY = 2;
    Spinner classesList;
    private String mCurrentPhotoPath;
    private int REQUEST_CAMERA = 786;
    private File compressedImage;
    DBHelper dbHelper;
    private Bitmap phtobitmap;
    private File actualImage;
    String randomNum;
    private String clgClasses,clgClasaId;
    private ImageView userImg;
    private String TAG=StaffRegFragment.class.getSimpleName();
    private byte[] userimg;
    private boolean isBackPressed=true;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_staff_reg,container,false);
        staticData=new StaticData(getActivity());
        eventTitle=view.findViewById(R.id.staffId);
        message=view.findViewById(R.id.staffName);
        register=view.findViewById(R.id.register);
        userImg=view.findViewById(R.id.userImg);
        commonMethod=new CommonMethod(getActivity());
        dbHelper=new DBHelper(getActivity());
        randomNum=commonMethod.randomNumber();
        register.setOnClickListener(this);
        userImg.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.register:
                validation();
                break;
            case R.id.userImg:
//                captureImg();
                chooseFromGallery();
                break;
        }
    }

    private void chooseFromGallery() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.setType("image/*");
        startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
    }

    private void validation() {
        String title=eventTitle.getText().toString();
        String msg=message.getText().toString();
        if (title.length()>0){
            eventTitle.setError(null);
            if (msg.length()>0){
                message.setError(null);

                Bitmap userBitmap=((BitmapDrawable) userImg.getDrawable()).getBitmap();
                Toast.makeText(getActivity(),"Register success",Toast.LENGTH_SHORT).show();
                dbHelper.insertEvent(title,msg,commonMethod.getBytes(userBitmap));
                setFragment(new StaffListFragment());

            }else {
                message.requestFocus();
                message.setError("required message");
            }
        }else {
            eventTitle.requestFocus();
            eventTitle.setError("required Title");
        }
    }

    private void captureImg() {
        Intent intent = new Intent( "android.media.action.IMAGE_CAPTURE" );
        actualImage = createImageFile();
        if (actualImage != null) {
            Uri photoURI;
            if (Build.VERSION.SDK_INT > 19) {
                photoURI = FileProvider.getUriForFile( getActivity(), "com.jp.fingerprintattendance.fileprovider", actualImage );
            } else {
                photoURI = Uri.fromFile( actualImage );
            }
            intent.putExtra( "output", photoURI );
            startActivityForResult( intent, REQUEST_CAMERA );
        }
    }
    private File createImageFile() {
        File mediaStorageDir = new File( Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DCIM ), "Camera" );
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdir();
        }
        File image = new File( mediaStorageDir, "IMG_" + new SimpleDateFormat( "yyyyMMdd_HHmmss" ).format( new Date() ) + ".jpg" );
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, "onActivityResult: " );
        if (requestCode == REQUEST_CAMERA) {
            Log.e(TAG, "onActivityResult: aaa" );
            customphotoCompressImage();
        }else if (requestCode == PICK_IMAGE_GALLERY) {
            if (data!=null) {
                Uri imageUri = data.getData();
                InputStream imageStream = null;
                try {
                    imageStream = Objects.requireNonNull(getActivity()).getContentResolver().openInputStream(Objects.requireNonNull(imageUri));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                userimg = commonMethod.getBytes(selectedImage);
                commonMethod.imageView(userImg, userimg, getActivity());
            }
        }
    }
    private void customphotoCompressImage() {
        if (actualImage != null) {
            try {
                compressedImage = new Compressor( getActivity() ).setMaxWidth( 540 ).setMaxHeight( 500 ).setQuality( 95 ).setCompressFormat( Bitmap.CompressFormat.JPEG ).compressToFile( actualImage );
                setphtoCompressedImage();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void setphtoCompressedImage() {
        FileNotFoundException e;
        phtobitmap = BitmapFactory.decodeFile( compressedImage.getAbsolutePath() );
        try {
            FileOutputStream outputStream = new FileOutputStream( actualImage );
            FileOutputStream fileOutputStream;
            phtobitmap.compress( Bitmap.CompressFormat.JPEG, 100, outputStream );
            fileOutputStream = outputStream;
        } catch (FileNotFoundException e3) {
            e = e3;
            e.printStackTrace();
//            userImg.setImageBitmap(phtobitmap);
            commonMethod.imageView( userImg, phtobitmap, getActivity() );
        }
//        userImg.setImageBitmap(phtobitmap);
        commonMethod.imageView( userImg, phtobitmap, getActivity() );
    }

    private void setFragment(Fragment _fragment) {
        // create a FragmentManager
        FragmentManager fm = getActivity().getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.framlayout1, _fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit(); // save the changes
    }

}
