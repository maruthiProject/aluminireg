package com.jp.aluminiregards.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jp.aluminiregards.R;
import com.jp.aluminiregards.adapter.StaffAdapter;
import com.jp.aluminiregards.db.DBHelper;
import com.jp.aluminiregards.model.StaffModel;

import java.util.ArrayList;

public class StaffListFragment extends Fragment implements View.OnClickListener {
    View view;
    TextView noData,dateSearch;
    RecyclerView recyclerViewList;
    StaffAdapter adapter;
    ArrayList<StaffModel> staffModelArrayList;
    DBHelper dbHelper;
FloatingActionButton fab;
    private boolean isBackPressed=true;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_staff_list,container,false);
        fab=view.findViewById(R.id.fab);
        fab.setOnClickListener(this);
        noData=view.findViewById(R.id.noData);
        dbHelper=new DBHelper(getActivity());
        recyclerViewList=view.findViewById(R.id.recyclerViewList);
        dateSearch=view.findViewById(R.id.dateSearch);
        dateSearch.setVisibility(View.GONE);
        recyclerViewList.setHasFixedSize(true);
        Bundle bundle=getArguments();
        if (bundle!=null){
            String status=bundle.getString("count");
            if (status.equals("1")) {
                fab.setVisibility(View.GONE);
            }else {
                fab.setVisibility(View.VISIBLE);
            }
            }
        recyclerViewList.setLayoutManager(new LinearLayoutManager(getActivity()));
        staffModelArrayList=dbHelper.getStaff();
        if (staffModelArrayList.size()>0){
            noData.setVisibility(View.GONE);
            recyclerViewList.setVisibility(View.VISIBLE);
        }
        adapter=new StaffAdapter(getActivity(),staffModelArrayList);
        recyclerViewList.setAdapter(adapter);
        handleBackPress(view);
        return view;
    }
    private void handleBackPress(View view) {

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (!isBackPressed) {
                        setFragment(new AdminHomeFragment());
                        isBackPressed = true;
                    } else if (isBackPressed) {
                        isBackPressed = false;
                    }
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab:
                setFragment(new StaffRegFragment());
                break;
        }
    }
    private void setFragment(Fragment _fragment) {
        // create a FragmentManager
        FragmentManager fm = getActivity().getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.framlayout1, _fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit(); // save the changes
    }
}
