package com.jp.aluminiregards.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jp.aluminiregards.R;
import com.jp.aluminiregards.adapter.StaffStudentAdapter;
import com.jp.aluminiregards.adapter.StudentAdapter;
import com.jp.aluminiregards.db.DBHelper;
import com.jp.aluminiregards.helper.CommonMethod;
import com.jp.aluminiregards.helper.DateListener;
import com.jp.aluminiregards.model.StudentModel;

import java.util.ArrayList;

public class StaffStudentListFragment extends Fragment implements View.OnClickListener, DateListener {
    View view;
    RecyclerView recyclerViewList;
    StudentAdapter adapter;
    StaffStudentAdapter adapterStaff;
    ArrayList<StudentModel> studentModelArrayList;
    DBHelper dbHelper;
    CommonMethod commonMethod;
    TextView noData,dateSearch;
    FloatingActionButton fab;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_staff_list,container,false);
        fab=view.findViewById(R.id.fab);
        fab.setOnClickListener(this);
        commonMethod=new CommonMethod(getActivity(),this);
        recyclerViewList=view.findViewById(R.id.recyclerViewList);
        recyclerViewList.setHasFixedSize(true);
        dbHelper=new DBHelper(getActivity());
        fab.setVisibility(View.GONE);
        dateSearch=view.findViewById(R.id.dateSearch);
        noData=view.findViewById(R.id.noData);
        recyclerViewList.setLayoutManager(new LinearLayoutManager(getActivity()));
        Bundle bundle=getArguments();
        if (bundle!=null){
            String status=bundle.getString("count");
            if (status.equals("1")){
                dateSearch.setVisibility(View.VISIBLE);
                studentModelArrayList=dbHelper.getStudentDaily(DBHelper.staffId,dbHelper.getSpcName(DBHelper.dbLogin,DBHelper.staffId),DBHelper.date,commonMethod.date());
                adapterStaff=new StaffStudentAdapter(getActivity(),studentModelArrayList);
                recyclerViewList.setAdapter(adapterStaff);
            }else {
                dateSearch.setVisibility(View.GONE);
                studentModelArrayList=dbHelper.getStudent(DBHelper.staffId,dbHelper.getSpcName(DBHelper.dbLogin,DBHelper.staffId));
                adapter=new StudentAdapter(getActivity(),studentModelArrayList);
                recyclerViewList.setAdapter(adapter);
            }
        }
        dateSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commonMethod.clickDate();
            }
        });


        if (studentModelArrayList.size()>0){
            noData.setVisibility(View.GONE);
            recyclerViewList.setVisibility(View.VISIBLE);
        }
        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fab:
                setFragment(new StudentRegFragment(),"1");
                break;
        }
    }
    private void setFragment(Fragment fragment, String count) {

        Bundle bundle=new Bundle();
        bundle.putString("count",count);
        fragment.setArguments(bundle);
        // create a FragmentManager
        FragmentManager fm = getActivity().getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.framlayout1, fragment);
        fragmentTransaction.commit(); // save the changes
    }

    @Override
    public void getDate(String date) {
        dateSearch.setText(date);
        studentModelArrayList=dbHelper.getStudentDaily(DBHelper.staffId,dbHelper.getSpcName(DBHelper.dbLogin,DBHelper.staffId),DBHelper.date,date);
        adapterStaff=new StaffStudentAdapter(getActivity(),studentModelArrayList);
        recyclerViewList.setAdapter(adapterStaff);
    }
}
