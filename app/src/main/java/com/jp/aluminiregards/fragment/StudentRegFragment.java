package com.jp.aluminiregards.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.jp.aluminiregards.activity.MasterActivity;
import com.jp.aluminiregards.R;
import com.jp.aluminiregards.db.DBHelper;
import com.jp.aluminiregards.helper.CommonMethod;
import com.jp.aluminiregards.helper.StaticData;
import com.jp.aluminiregards.model.StudentModel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import id.zelory.compressor.Compressor;

public class StudentRegFragment extends Fragment implements View.OnClickListener {
    View view;
    StaticData staticData;
    ArrayList<String> classes = new ArrayList<>();
    ArrayList<String> classId = new ArrayList<>();
    ArrayAdapter<String> classesAdapter;
    EditText staffId, studentName, password, studentEmail, studentId, department, yearPass, compyHrstudy, mobile, address,dob;
    CommonMethod commonMethod;
    TextView register;
    ImageView userImg;
    private String mCurrentPhotoPath;
    private int REQUEST_CAMERA = 786;
    private File actualImage;
    private File compressedImage;
    private Bitmap phtobitmap;
    DBHelper dbHelper;
    Spinner classesList;
    String randomNum;
    RelativeLayout clickfalse;
    private String status, clgClasaId;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_student_reg, container, false);
        staticData = new StaticData(getActivity());
        studentId = view.findViewById(R.id.studentId);
        staffId = view.findViewById(R.id.staffId);
        clickfalse=view.findViewById(R.id.clickfalse);
        studentName = view.findViewById(R.id.studentName);
        studentEmail = view.findViewById(R.id.studentEmail);
        password = view.findViewById(R.id.password);
        userImg = view.findViewById(R.id.userImg);
        dob=view.findViewById(R.id.dob);
        classesList = view.findViewById(R.id.classes);
        register = view.findViewById(R.id.register);
        mobile = view.findViewById(R.id.mobile);
        department = view.findViewById(R.id.department);
        address = view.findViewById(R.id.address);
        yearPass = view.findViewById(R.id.yearPass);
        compyHrstudy = view.findViewById(R.id.compyHrstudy);
        commonMethod = new CommonMethod(getActivity());
        dbHelper = new DBHelper(getActivity());
        randomNum = commonMethod.randomNumber();
        studentId.setText("STUID" + randomNum);
        Bundle bundle=getArguments();
        if (bundle!=null){
            staffId.setClickable(false);
            staffId.setFocusable(false);
            studentEmail.setFocusable(false);
            studentEmail.setClickable(false);
            studentName.setFocusable(false);
            studentName.setClickable(false);
            password.setFocusable(false);
            password.setClickable(false);
            dob.setFocusable(false);
            dob.setClickable(false);
            department.setFocusable(false);
            department.setClickable(false);
            address.setFocusable(false);
            address.setClickable(false);
            yearPass.setFocusable(false);
            yearPass.setClickable(false);
            mobile.setClickable(false);
            mobile.setFocusable(false);
            compyHrstudy.setClickable(false);
            compyHrstudy.setFocusable(false);
            userImg.setClickable(false);
            userImg.setFocusable(false);
            StudentModel studentModel= (StudentModel) bundle.getSerializable("model");
           studentId.setText(studentModel.getUserId());
           studentEmail.setText(studentModel.getEmail());
            studentName.setText(studentModel.getStudentName());
           password.setText(studentModel.getStudentPassword());
            dob.setText(studentModel.getDob());
            department.setText(studentModel.getDepart());
            address.setText(studentModel.getAddress());
             yearPass.setText(studentModel.getYearPasssed());
            mobile.setText(studentModel.getMobileNo());
            compyHrstudy.setText(studentModel.getCompanyHr());
            commonMethod.imageView(userImg, studentModel.getImage(),getActivity() );
            register.setVisibility(View.GONE);

        }else {
            register.setOnClickListener(this);
            userImg.setOnClickListener(this);
            yearPass.setOnClickListener(this);
            dob.setOnClickListener(this);
        }
        return view;
    }

    private void validation() {
        String studentID = studentId.getText().toString();
        String studentEmailId = studentEmail.getText().toString();
        String clgStudentName = studentName.getText().toString();
        String pass = password.getText().toString();
        String dateOf=dob.getText().toString();
        String userDepartment = department.getText().toString();
        String userAddress = address.getText().toString();
        String yearOfPasssedOut = yearPass.getText().toString();
        String mobileno = mobile.getText().toString();
        String cmpyHr = compyHrstudy.getText().toString();
        if (clgStudentName.length() > 0) {
            studentName.setError(null);
            if (dateOf.length()>0){
                dob.setError(null);

            if (userDepartment.length() > 0) {
                department.setError(null);
                if (yearOfPasssedOut.length() > 0) {
                    yearPass.setError(null);
                    if (cmpyHr.length() > 0) {
                        compyHrstudy.setError(null);
                        studentName.setError(null);
                        if (studentEmailId.length() > 0) {
                            studentEmail.setError(null);
                            if (commonMethod.EmailValid(studentEmailId)) {
                                studentEmail.setError(null);
                                if (pass.length() > 0) {
                                    password.setError(null);

                                    Bitmap userBitmap = ((BitmapDrawable) userImg.getDrawable()).getBitmap();
                                    Toast.makeText(getActivity(), "Register success", Toast.LENGTH_SHORT).show();
                                    dbHelper.insertstudent(studentID, userDepartment, clgStudentName, studentEmailId, pass, commonMethod.getBytes(userBitmap)
                                            , yearOfPasssedOut, mobileno, cmpyHr, userAddress,dateOf);
                                    if (status.equals("1")) {
                                        setFragment(new StudentListFragment());
                                    }else {
                                        startActivity(new Intent(getActivity(), MasterActivity.class));
                                        getActivity().finish();
                                    }

                                } else {
                                    password.requestFocus();
                                    password.setError("required Password");
                                }
                            } else {
                                studentEmail.requestFocus();
                                studentEmail.setError("valid Email");
                            }
                        } else {
                            studentEmail.requestFocus();
                            studentEmail.setError("required Email");
                        }
                    } else {
                        compyHrstudy.requestFocus();
                        compyHrstudy.setError("required Company name or higher study");
                    }
                } else {
                    yearPass.requestFocus();
                    yearPass.setError("choose year");
                }
            } else {
                department.requestFocus();
                department.setError("required department");
            }
            }else {
                dob.requestFocus();
                dob.setError("required date of birth");
            }
        } else {
            studentName.requestFocus();
            studentName.setError("required student Name");
        }
    }

    private void setFragment(Fragment _fragment) {
        // create a FragmentManager
        FragmentManager fm = getActivity().getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.framlayout1, _fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit(); // save the changes
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.register:
                validation();
                break;
            case R.id.userImg:
                captureImg();
                break;
            case R.id.yearPass:
                commonMethod.monthPickerDilog(yearPass);
                break;
            case R.id.dob:
                commonMethod.clickDate(dob);
                break;
        }
    }

    private void captureImg() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        actualImage = createImageFile();
        if (actualImage != null) {
            Uri photoURI;
            if (Build.VERSION.SDK_INT > 19) {
                photoURI = FileProvider.getUriForFile(getActivity(), "com.jp.aluminiregards.fileprovider", actualImage);
            } else {
                photoURI = Uri.fromFile(actualImage);
            }
            intent.putExtra("output", photoURI);
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    private File createImageFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), "Camera");
        if (!mediaStorageDir.exists()) {
            mediaStorageDir.mkdir();
        }
        File image = new File(mediaStorageDir, "IMG_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".jpg");
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA) {
            customphotoCompressImage();
        }
    }

    private void customphotoCompressImage() {
        if (actualImage != null) {
            try {
                compressedImage = new Compressor(getActivity()).setMaxWidth(540).setMaxHeight(500).setQuality(95).setCompressFormat(Bitmap.CompressFormat.JPEG).compressToFile(actualImage);
                setphtoCompressedImage();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void setphtoCompressedImage() {
        FileNotFoundException e;
        phtobitmap = BitmapFactory.decodeFile(compressedImage.getAbsolutePath());
        try {
            FileOutputStream outputStream = new FileOutputStream(actualImage);
            FileOutputStream fileOutputStream;
            phtobitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            fileOutputStream = outputStream;
        } catch (FileNotFoundException e3) {
            e = e3;
            e.printStackTrace();

            commonMethod.imageView(userImg, phtobitmap, getActivity());
        }

        commonMethod.imageView(userImg, phtobitmap, getActivity());
    }

}
