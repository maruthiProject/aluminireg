package com.jp.aluminiregards.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jp.aluminiregards.R;
import com.jp.aluminiregards.db.DBHelper;
import com.jp.aluminiregards.helper.CommonMethod;

public class StudentHomeFragment extends Fragment {

    View view;
    ImageView attendanceStatus;
    DBHelper dbHelper;
    CommonMethod commonMethod;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view=inflater.inflate(R.layout.fragment_student_home,container,false);
        attendanceStatus=view.findViewById(R.id.attendanceStatus);
        commonMethod=new CommonMethod(getActivity());
        dbHelper=new DBHelper(getActivity());
        /*if (dbHelper.getSpcName(DBHelper.dbStudentDaily,DBHelper.attendance,DBHelper.userId,dbHelper.getSpcName(DBHelper.dbLogin,DBHelper.userId),
                DBHelper.date,commonMethod.date()).equals("P")){
            attendanceStatus.setImageResource(R.drawable.att_maked);
        }else {
            attendanceStatus.setImageResource(R.drawable.att_not_marked);
        }*/
        return view;
    }
}
