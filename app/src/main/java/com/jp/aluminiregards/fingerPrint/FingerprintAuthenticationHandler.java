package com.jp.aluminiregards.fingerPrint;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.jp.aluminiregards.R;
import com.jp.aluminiregards.activity.StudentHomeActivity;
import com.jp.aluminiregards.db.DBHelper;
import com.jp.aluminiregards.helper.CommonMethod;


@RequiresApi(api = Build.VERSION_CODES.M)
public class FingerprintAuthenticationHandler extends FingerprintManager.AuthenticationCallback {


    private Context context;
    DBHelper dbHelper;
    CommonMethod commonMethod;

    // Constructor
    public FingerprintAuthenticationHandler(Context mContext) {
        context = mContext;
        dbHelper=new DBHelper(context);
        commonMethod=new CommonMethod(context);
    }


    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        CancellationSignal cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }


    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        this.update("Fingerprint Authentication error\n" + errString, false);
    }


    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        this.update("Fingerprint Authentication help\n" + helpString, false);
    }


    @Override
    public void onAuthenticationFailed() {
        this.update("Fingerprint Authentication failed.", false);
    }


    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        this.update("Fingerprint Authentication succeeded.", true);
    }


    public void update(String e, Boolean success){
        TextView textView = (TextView) ((Activity)context).findViewById(R.id.errorText);
        textView.setText(e);
        if(success){
            dbHelper.updatestudentDaily(dbHelper.getSpcName(DBHelper.dbLogin,DBHelper.userId),commonMethod.date(),"P");
            textView.setTextColor(ContextCompat.getColor(context,R.color.colorPrimaryDark));
            Intent i = new Intent(context, StudentHomeActivity.class);
            context.startActivity(i);
            ((Activity) context).finish();
        }
    }
    private void setFragment(Fragment fragment, Fragment fragment1) {
        // create a FragmentManager
        FragmentManager fm = fragment1.getActivity().getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.framlayout, fragment);
        fragmentTransaction.commit(); // save the changes
    }
}