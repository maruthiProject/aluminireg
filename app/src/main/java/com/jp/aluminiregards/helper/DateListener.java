package com.jp.aluminiregards.helper;

public interface DateListener {
    void getDate(String date);
}
