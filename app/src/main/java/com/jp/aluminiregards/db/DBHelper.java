package com.jp.aluminiregards.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.jp.aluminiregards.model.StaffModel;
import com.jp.aluminiregards.model.StudentModel;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {
    private static int version=1;
    private static String DB_NAME="fingerPrint";
    public static String dbLogin="login";
    public static String dbStaff="staff";
    public static String dbStudent="student";
    public static String dbEvents="event";
    public static String c_Id="id";
    public static String userId="userId";
    public static String title="eventTitle";
    public static String message="message";
    public static String department="department";
    public static String yearPassed="yserPassed";
    public static String moileNo="mobileNo";
    public static String userType="userType";
    public static String user_Name="name";
    public static String e_Mail="email";
    public static String user_Password="password";
    public static String staffId="staffId";
    public static String address="address";
    private static String image="image";
    public static String compyOrHrStydy="companyOrHigher";
    public static String date="date";
    private String TAG=DBHelper.class.getSimpleName();

    public DBHelper(Context context) {
        super(context,DB_NAME,null,version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String queryLogin = "CREATE TABLE " + dbLogin + "(" + c_Id + " INTEGER, " + userId + " TEXT," + staffId + " TEXT," + user_Name + " TEXT,"+ userType + " TEXT,"
                + e_Mail + " TEXT," + user_Password + " TEXT)";
        db.execSQL( queryLogin );

        String queryStudent = "CREATE TABLE " + dbStudent + "(" + c_Id + " INTEGER, " + userId + " TEXT," + department + " TEXT,"+ yearPassed + " TEXT," + moileNo + " TEXT,"
                + user_Name + " TEXT," + e_Mail + " TEXT,"   + address + " TEXT," + date + " TEXT," + compyOrHrStydy + " TEXT," + image + " BLOB," + user_Password + " TEXT)";
        db.execSQL( queryStudent );
        String queryStudentDaily = "CREATE TABLE " + dbEvents + "(" + c_Id + " INTEGER, " + title + " TEXT," + message + " TEXT," + image + " BLOB )";
        db.execSQL( queryStudentDaily );

       /* String queryStaff = "CREATE TABLE " + dbStaff + "(" + c_Id + " INTEGER, " + " TEXT," + staffId + " TEXT," + user_Name + " TEXT," + classes + " TEXT,"
                + image + " BLOB," + user_Password + " TEXT)";
        db.execSQL( queryStaff );
*/

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL( "DROP TABLE " + dbLogin );
        db.execSQL( "DROP TABLE " + dbEvents );
        db.execSQL( "DROP TABLE " + dbStudent );
        onCreate(db);
    }

    //Inserting data from Login API
    public void insertLogin(String id, String name, String email, String password, String userttype) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put( userId, id );
        values.put( user_Name, name );
        values.put( userType, userttype );
        values.put( e_Mail, email );
        values.put( user_Password, password );
        db.insert( dbLogin, null, values );
        db.close();
    }
    //Inserting data from Login API
    public void insertEvent(String eventTitle, String msg, byte[] byeImg) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(title,eventTitle);
        values.put( message, msg );
        values.put( image, byeImg );
//        values.put(classes,classesSelected);
        db.insert( dbEvents, null, values );
        db.close();
    }
    //Inserting data from Login API
    public void insertstudent(String id, String userDepartment, String name, String email, String password, byte[] byeImg
            ,String yearpassed,String mobileNo,String companyHr,String userAddress,String toDate) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put( userId, id );
        values.put( user_Name, name );
        values.put( e_Mail, email );
        values.put( user_Password, password );
        values.put( department, userDepartment );
        values.put( yearPassed, yearpassed );
        values.put( department, userDepartment );
        values.put( moileNo, mobileNo );
        values.put( image, byeImg );
        values.put( address, userAddress );
        values.put(compyOrHrStydy,companyHr);
        values.put( date, toDate );
        db.insert( dbStudent, null, values );
        db.close();
    }
    //Inserting data from Login API
    public void insertstudentDaily(String id, String staffID, String name, String email, String password, byte[] byeImg, String classesSelected,
                                   String studAttendance,String toDate) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put( userId, id );
        values.put( staffId, staffID );
        values.put( user_Name, name );
        values.put( e_Mail, email );
//        values.put( attendance, studAttendance );
        values.put( user_Password, password );
        values.put( image, byeImg );
        values.put( date, toDate );
//        values.put(classes,classesSelected);
        db.insert( "", null, values );
        db.close();
    }
    //Inserting data from Login API
    public void updatestudent(String id, String staffID, String name, String email, String password, byte[] byeImg, String classesSelected,String studAttendance) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put( staffId, staffID );
        values.put( user_Name, name );
        values.put( e_Mail, email );
//        values.put( attendance, studAttendance );
        values.put( user_Password, password );
        values.put( image, byeImg );
//        values.put(classes,classesSelected);
        db.update( dbStudent, values,userId + "=?",new String[]{id} );
        db.close();
    }
    //Inserting data from Login API
    public void updatestudentDaily(String id,String stuDate, String studAttendance) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
//        values.put( attendance, studAttendance );
        db.update( "", values,userId + "=? and " + date + " =? ",new String[]{id,stuDate} );
        db.close();
    }

    public ArrayList<StaffModel> getStaff(){
        ArrayList<StaffModel> staffModelArrayList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + dbEvents;
        Log.e(TAG, "getStaff: "+ selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery( selectQuery, null );

        if (c.moveToFirst()) {
            do {
                String userTitle = c.getString(c.getColumnIndex(title));
                String msg = c.getString(c.getColumnIndex(message));
                byte[] staffImage = c.getBlob(c.getColumnIndex(image));
                staffModelArrayList.add(new StaffModel(userTitle,msg,staffImage));
            }while (c.moveToNext());
        }
        return staffModelArrayList;
    }

    public ArrayList<StaffModel> getStaff(String key, String value,String key1, String value1){
        ArrayList<StaffModel> staffModelArrayList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + dbStaff + " WHERE " + key + " = '" + value +"' and " + key1 + " = '" + value1 + "'";
        Log.e(TAG, "getStudent: "+ selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery( selectQuery, null );

        if (c.moveToFirst()) {
            do {
                String staffID = c.getString(c.getColumnIndex(staffId));
                String staffName = c.getString(c.getColumnIndex(user_Name));
                String staffPassword = c.getString(c.getColumnIndex(user_Password));
//                String selectedClass = c.getString(c.getColumnIndex(classes));
                byte[] staffImage = c.getBlob(c.getColumnIndex(image));
//                staffModelArrayList.add(new StaffModel(staffID,staffName,staffPassword,selectedClass,staffImage));
            }while (c.moveToNext());
        }
        return staffModelArrayList;
    }

    public ArrayList<StudentModel> getStudent(){
        ArrayList<StudentModel> studentModelArrayList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + dbStudent;
        Log.e(TAG, "getStudent: "+ selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery( selectQuery, null );

        if (c.moveToFirst()) {
            do {
                String studentId = c.getString(c.getColumnIndex(userId));
                String studentName = c.getString(c.getColumnIndex(user_Name));
                String email = c.getString(c.getColumnIndex(e_Mail));
                String userAddress = c.getString(c.getColumnIndex(address));
                String studentPassword = c.getString(c.getColumnIndex(user_Password));
                String phoneNumber = c.getString(c.getColumnIndex(moileNo));
                String companyHr= c.getString(c.getColumnIndex(compyOrHrStydy));
                String userDept = c.getString(c.getColumnIndex(department));
                String yearPass= c.getString(c.getColumnIndex(yearPassed));
                String dob= c.getString(c.getColumnIndex(date));
                byte[] studentImage = c.getBlob(c.getColumnIndex(image));
                studentModelArrayList.add(new StudentModel(studentId,studentName,studentPassword,userAddress,email,yearPass,companyHr,phoneNumber,studentImage,dob,userDept));
            }while (c.moveToNext());
        }
        return studentModelArrayList;
    }
    public ArrayList<StudentModel> getStudentDaily(String key,String value,String key1,String value1){
        ArrayList<StudentModel> studentModelArrayList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + "" + " WHERE " + key + " = '" +value + "' and "+ key1 + " = '" +value1 + "'" ;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery( selectQuery, null );
        Log.e(TAG, "getStudentDaily: "+ selectQuery);
        if (c.moveToFirst()) {
            do {
                String studentId = c.getString(c.getColumnIndex(userId));
                String staffID = c.getString(c.getColumnIndex(staffId));
                String studentName = c.getString(c.getColumnIndex(user_Name));
                String email = c.getString(c.getColumnIndex(e_Mail));
                String studentPassword = c.getString(c.getColumnIndex(user_Password));
//                String selectedClass = c.getString(c.getColumnIndex(classes));
                String toDate = c.getString(c.getColumnIndex(date));
//                String stuAddentance = c.getString(c.getColumnIndex(attendance));
                byte[] studentImage = c.getBlob(c.getColumnIndex(image));
//                studentModelArrayList.add(new StudentModel(studentId,staffID,studentName,studentPassword,selectedClass,email,toDate,stuAddentance,studentImage));
            }while (c.moveToNext());
        }
        return studentModelArrayList;
    }

    public ArrayList<StudentModel> getStudent(String key, String value){
        ArrayList<StudentModel> studentModelArrayList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + dbStudent + " WHERE " + key + " = '" + value +"' ";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery( selectQuery, null );

        if (c.moveToFirst()) {
            do {
                String studentId = c.getString(c.getColumnIndex(userId));
                String staffID = c.getString(c.getColumnIndex(staffId));
                String studentName = c.getString(c.getColumnIndex(user_Name));
                String email = c.getString(c.getColumnIndex(e_Mail));
                String studentPassword = c.getString(c.getColumnIndex(user_Password));
//                String selectedClass = c.getString(c.getColumnIndex(classes));
//                String studAtt= c.getString(c.getColumnIndex(attendance));
                byte[] studentImage = c.getBlob(c.getColumnIndex(image));
//                studentModelArrayList.add(new StudentModel(studentId,staffID,studentName,studentPassword,selectedClass,email,studAtt,studentImage));
            }while (c.moveToNext());
        }
        return studentModelArrayList;
    }
    public ArrayList<StudentModel> getStudent(String key, String value,String key1, String value1){
        ArrayList<StudentModel> studentModelArrayList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + dbStudent + " WHERE " + key + " = '" + value +"' and " + key1 + " = '" + value1 + "'";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery( selectQuery, null );

        if (c.moveToFirst()) {
            do {
                String studentId = c.getString(c.getColumnIndex(userId));
                String staffID = c.getString(c.getColumnIndex(staffId));
                String studentName = c.getString(c.getColumnIndex(user_Name));
                String email = c.getString(c.getColumnIndex(e_Mail));
                String studentPassword = c.getString(c.getColumnIndex(user_Password));
//                String selectedClass = c.getString(c.getColumnIndex(classes));
                byte[] studentImage = c.getBlob(c.getColumnIndex(image));
//                String studAtt= c.getString(c.getColumnIndex(attendance));
//                studentModelArrayList.add(new StudentModel(studentId,staffID,studentName,studentPassword,selectedClass,email,studAtt,studentImage));
            }while (c.moveToNext());
        }
        return studentModelArrayList;
    }

    public boolean checkData(String tableName, String key, String value,String key1, String value1) {
        SQLiteDatabase db = getWritableDatabase();
        String Query = "Select * from " + tableName + " where " + key + " = '" + value + "' and " + key1 + " = '" + value1 + "'";
        //  Log.e(TAG, "check data: " + Query);
        Cursor cursor = db.rawQuery(Query, null);
        boolean status = false;
        status = cursor.getCount() > 0;
        cursor.close();
        db.close();
        return status;
    }
    public boolean checkData(String tableName) {
        SQLiteDatabase db = getWritableDatabase();
        String Query = "Select * from " + tableName ;
        //  Log.e(TAG, "check data: " + Query);
        Cursor cursor = db.rawQuery(Query, null);
        boolean status = false;
        status = cursor.getCount() > 0;
        cursor.close();
        db.close();
        return status;
    }

    public String getSpcName(String tableName, String keyName) {
        String name = "";
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + keyName + " FROM " + tableName;
        Cursor c = db.rawQuery(query, null);
        if (c.getCount() > 0) {
            c.moveToFirst();
            name = c.getString(c.getColumnIndex(keyName));
        }
        c.close();
        db.close();
        return name;
    }
    public String getSpcName(String tableName, String keyName,String key,String value,String key1,String value1) {
        String name = "";
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + keyName + " FROM " + tableName + " WHERE " + key + " = '" + value + "' and "
                + key1 + " = '" + value1 + "'";
        Cursor c = db.rawQuery(query, null);
        if (c.getCount() > 0) {
            c.moveToFirst();
            name = c.getString(c.getColumnIndex(keyName));
        }
        c.close();
        db.close();
        return name;
    }

    public void delete(String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + tableName;
        db.execSQL(query);
        db.close();
    }
}
