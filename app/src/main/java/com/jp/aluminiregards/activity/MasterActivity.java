package com.jp.aluminiregards.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.jp.aluminiregards.R;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;

import at.markushi.ui.CircleButton;

public class MasterActivity extends AppCompatActivity {

    CircleButton mCirecularButtonMembers,mCirecularButtonLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_master);
        // setting icons to the circular buttons
        mCirecularButtonMembers=findViewById(R.id.circleButton_member);
        mCirecularButtonLogin=findViewById(R.id.circleButton_event);

        mCirecularButtonMembers.setImageDrawable(new IconicsDrawable(this)
                .icon(FontAwesome.Icon.faw_users)
                .color(Color.WHITE)
                .sizeDp(25));
      mCirecularButtonLogin.setImageDrawable(getResources().getDrawable(R.drawable.ic_right_arrow));
      mCirecularButtonLogin.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              startActivity(new Intent(MasterActivity.this, LoginActivity.class));
              finish();
          }
      });
      mCirecularButtonMembers.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View view) {
              startActivity(new Intent(MasterActivity.this, StudentHomeActivity.class));
          }
      });
    }
}
