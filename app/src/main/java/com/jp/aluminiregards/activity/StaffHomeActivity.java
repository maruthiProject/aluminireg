package com.jp.aluminiregards.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.jp.aluminiregards.R;
import com.jp.aluminiregards.fragment.StaffListFragment;
import com.jp.aluminiregards.fragment.StaffStudentListFragment;
import com.jp.aluminiregards.helper.CommonMethod;

public class StaffHomeActivity extends AppCompatActivity {

CommonMethod commonMethod;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    setFragment(new StaffStudentListFragment(),"1");
                    return true;
                case R.id.navigation_dashboard:
                    setFragment(new StaffStudentListFragment(),"2");
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff_home);
        commonMethod=new CommonMethod(this);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        setFragment(new StaffListFragment(),"1");
        setTitle("Event List");
    }
    private void setFragment(Fragment fragment, String count) {

        Bundle bundle=new Bundle();
        bundle.putString("count",count);
        fragment.setArguments(bundle);
        // create a FragmentManager
        FragmentManager fm = getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.framlayoutStaff, fragment);
        fragmentTransaction.commit(); // save the changes
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        android.app.Fragment fragment = null;
        String title = getString(R.string.app_name);
        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            commonMethod.Logoutscreen();
            //    Toast.makeText(getApplicationContext(), " action is Cart!", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}
