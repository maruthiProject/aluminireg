package com.jp.aluminiregards.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.jp.aluminiregards.R;
import com.jp.aluminiregards.fragment.AdminHomeFragment;
import com.jp.aluminiregards.helper.CommonMethod;

public class AdminHomeActivity extends AppCompatActivity {

    CommonMethod commonMethod;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_home);
        commonMethod=new CommonMethod(this);
        setFragment(new AdminHomeFragment());
    }
    private void setFragment(Fragment _fragment) {
        // create a FragmentManager
        FragmentManager fm = getSupportFragmentManager();
        // create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        // replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.framlayout1, _fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit(); // save the changes
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        android.app.Fragment fragment = null;
        String title = getString(R.string.app_name);
        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            commonMethod.Logoutscreen();
            //    Toast.makeText(getApplicationContext(), " action is Cart!", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}
